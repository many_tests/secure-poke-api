import re

from flask import Blueprint, request, jsonify

from services.poke import PokeAPI
from services.poke.exception import PokeTypeNotFound, PokemonNotFound

poke_bp = Blueprint("poke_bp", __name__, url_prefix="/api/pokemon")


@poke_bp.route('/<pokemon>', methods=['GET'])
def get_pokemon_details(pokemon: str):
    try:
        response = PokeAPI().get_pokemon(pokemon)
    except PokemonNotFound as e:
        return jsonify({"details": e.message}), 404
    return jsonify(response["data"]), 200


@poke_bp.route('/', methods=['GET'])
def list_current_user_pokemons():
    if "types" not in request.args.keys():
        return {"details": "Please give a valid type for get pokemons"}, 400
    types = []
    for match in re.finditer(r"(?P<type>\w*).?", request.args["types"]):
        if match.group("type"):
            types.append(match.group("type"))
    try:
        response = PokeAPI().get_pokemons(types)
    except PokeTypeNotFound as e:
        return jsonify({"details": e.message}), 404
    return jsonify(response["data"]), 200
