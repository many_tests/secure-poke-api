from flask import Flask

from core.config import get_configuration


def create_app(**kwargs):
    """
    Create App instance with bdd connection and blueprints registering

    :return: app
    """
    # create and configure the app
    _app = Flask(__name__, instance_relative_config=True)

    # Load config
    _app.config.from_object(get_configuration(_app.config["ENV"]))
    _app.url_map.strict_slashes = False

    # Init Resources
    from routers.pokemon import poke_bp
    _app.register_blueprint(poke_bp)

    return _app


if __name__ == "__main__":
    app = create_app()
