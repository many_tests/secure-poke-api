"""
All environment Config
"""
import os
from pydantic import BaseSettings
from enum import Enum


class EnvironmentEnum(str, Enum):
    """
    Enum with available environments
    """

    PRODUCTION = "production"
    DEVELOPMENT = "development"


class GlobalConfig(BaseSettings):
    """
    Global config for override in relation of env
    """

    TITLE: str = "Secure Poké API server"
    DESCRIPTION: str = "Web service to manage pokemon through PokeAPI"
    VERSION: str = "0.0.1"

    BASE_DIR: str = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    DEBUG: bool = False
    SECRET_KEY: str

    class Config:
        case_sensitive = True


class DevelopmentConfig(GlobalConfig):
    """Development configurations."""

    DEBUG: bool = True
    SECRET_KEY: str = "very_secret"


class ProductionConfig(GlobalConfig):
    """Production configurations."""

    DEBUG: bool = False
    SECRET_KEY: str = os.getenv("SECRET", "very_secret")


def get_configuration(env) -> GlobalConfig:
    """
    Instantiation of configuration from env variable
    """
    if env == EnvironmentEnum.PRODUCTION.value:
        return ProductionConfig()
    return DevelopmentConfig()
