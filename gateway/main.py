from flask import Flask

from core.config import GlobalConfig
from router.dispatcher import bp


def create_app(**kwargs) -> Flask:
    """
    Create App instance with bdd connection and blueprints registering

    :return: app
    """
    # create and configure the app
    _app = Flask(__name__, instance_relative_config=True)

    # Load config
    _app.config.from_object(GlobalConfig())
    _app.url_map.strict_slashes = False

    # Register router
    _app.register_blueprint(bp)

    return _app


if __name__ == "__main__":
    app = create_app()
