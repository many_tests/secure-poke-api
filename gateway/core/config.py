"""
All environment Config
"""
import os
from pydantic import BaseSettings


class GlobalConfig(BaseSettings):
    """
    Global config for override in relation of env
    """

    TITLE: str = "Secure Poké API Gateway server"
    DESCRIPTION: str = "Server to centralize requests to provide a single endpoint"
    VERSION: str = "0.0.1"

    BASE_DIR: str = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    DEBUG: bool = False

    AUTH_SERVICE: str = "http://auth:5000/api"
    POKE_SERVICE: str = "http://app:5001/api"

    RESOURCES: dict[str, list] = {
        "auth": ["signup", "login", "group", "user"],
        "poke": ["pokemon"],
    }

    class Config:
        case_sensitive = True
