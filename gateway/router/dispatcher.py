import json

import requests
from flask import Blueprint, current_app, request, jsonify

bp = Blueprint("bp", __name__)

AUTH_SERVICE = 'AUTH_SERVICE'
POKE_SERVICE = 'POKE_SERVICE'


@bp.route("/api/signup", methods=['POST'])
def signup():
    response = requests.post(f"{current_app.config.get(AUTH_SERVICE)}/signup", headers=request.headers,
                             data=json.dumps(request.get_json(force=True)))
    return jsonify(response.json()), response.status_code


@bp.route("/api/login", methods=['POST'])
def login():
    response = requests.post(f"{current_app.config.get(AUTH_SERVICE)}/login", headers=request.headers,
                             data=json.dumps(request.get_json(force=True)))
    return jsonify(response.json()), response.status_code


@bp.route("/api/user/me", methods=['GET'])
def me():
    response = requests.get(f"{current_app.config.get(AUTH_SERVICE)}/user/me", headers=request.headers)
    if response.status_code != 200:
        return jsonify(response.json()), response.status_code
    return jsonify(response.json()), response.status_code


@bp.route('/api/group/<group>/add', methods=['POST'])
def add_to_group(group: str):
    response = requests.post(f"{current_app.config.get(AUTH_SERVICE)}/group/{group}/add", headers=request.headers)
    if response.status_code != 200:
        return jsonify(response.json()), response.status_code
    return jsonify(response.json()), response.status_code


@bp.route('/api/group/<group>/remove', methods=['POST'])
def remove_to_group(group: str):
    response = requests.post(f"{current_app.config.get(AUTH_SERVICE)}/group/{group}/remove", headers=request.headers)
    if response.status_code != 200:
        return jsonify(response.json()), response.status_code
    return jsonify(response.json()), response.status_code


@bp.route("/api/pokemon", methods=['GET'])
def get_pokemons():
    response = requests.get(f"{current_app.config.get(AUTH_SERVICE)}/user/me", headers=request.headers)
    if response.status_code == 401:
        return jsonify({"details": "Unauthorized, please log in"}), 401
    groups = [grp["name"] for grp in response.json()["groups"]]
    response = requests.get(f"{current_app.config.get(POKE_SERVICE)}/pokemon/?types={','.join(groups)}")
    if response.status_code != 200:
        return jsonify(response.json()), response.status_code
    return jsonify(response.json()), response.status_code


@bp.route("/api/pokemon/<pokemon>", methods=['GET'])
def get_pokemon(pokemon: str):
    response = requests.get(f"{current_app.config.get(AUTH_SERVICE)}/user/me", headers=request.headers)
    if response.status_code == 401:
        return jsonify({"details": "Unauthorized, please log in"}), 401
    user = response.json()
    response = requests.get(f"{current_app.config.get(POKE_SERVICE)}/pokemon/{pokemon}")
    if response.status_code != 200:
        return jsonify(response.json()), response.status_code
    for type_data in response.json()["types"]:
        if type_data["type"]["name"] not in [grp["name"] for grp in user["groups"]]:
            return jsonify({"detail": f"You haven't joined one of {pokemon}'s type group."}), response.status_code
    return jsonify(response.json()), response.status_code
