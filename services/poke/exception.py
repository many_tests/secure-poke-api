class PokeTypeNotFound(Exception):
    def __init__(self, name: str):
        self.message = f"Type \"{name}\" is not available"


class PokemonNotFound(Exception):
    def __init__(self, name: str):
        self.message = f"Pokemon \"{name}\" is not exist"
