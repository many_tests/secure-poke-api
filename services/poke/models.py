from typing import Optional

from pydantic import BaseModel


class PokemonSchema(BaseModel):
    name: str
    groups: list[str]


class PokeTypeSchema(BaseModel):
    name: str
    pokemon: Optional[list[PokemonSchema]]
