import requests

from .exception import PokeTypeNotFound, PokemonNotFound
from .models import PokeTypeSchema


class PokeAPI:
    base_url = "https://pokeapi.co/api/v2"

    def get_type(self, name: str, with_details: bool = False) -> dict:
        response = requests.get(f"{self.base_url}/type/{name}")
        if response.status_code == 404:
            raise PokeTypeNotFound(name)
        if with_details:
            return {"status": response.status_code, "data": response.json()}
        return {"status": response.status_code, "data": PokeTypeSchema(name=response.json().get("name"))}

    def get_pokemon(self, name: str) -> dict:
        response = requests.get(f"{self.base_url}/pokemon/{name}")
        if response.status_code == 404:
            raise PokemonNotFound(name)

        return {"status": response.status_code, "data": response.json()}

    def get_pokemons(self, groups: list) -> dict:
        pokemons = {}
        for group in groups:
            response = self.get_type(group, with_details=True)
            if response["status"] == 200:
                if group not in pokemons.keys():
                    pokemons[group] = []
                pokemons[group].extend([pokemon["pokemon"]["name"] for pokemon in response["data"]["pokemon"]])
            else:
                raise PokeTypeNotFound(group)
        return {"status": 200, "data": pokemons}
