# Secure Poke API

## Objective

the objective of this app is to set up a server to earn pokemons with authentication service.

## Use case

1) You have to create your account : /api/signup
2) You have to provide your credentials for access to the other services : /api/login
3) You have to belong to pokemon type group for get visibility to all pokemon of this group : /api/group/{{type}}/add
4) You can leave a pokemon type group : /api/group/{{type}}/remove
5) You can list all pokemon of yours groups : /api/pokemon
6) You can get details of a pokemon if it has one of your group as type : /api/pokemon/{{pokemon}}

## Getting started

For launch this application you must install `docker` and `docker-compose`.

Next, you have to run the following command to launch this application :

    $ docker-compose up --build

You can now access to pgAdmin serveur on http://localhost:8080/ and to application on http://localhost/

## Documentation API

### <u>Signup</u>

Route: (POST) http://localhost/api/signup

Body :

    {
        "name": string,
        "username": string,
        "password": string,
        "password_confirm": string
    }

- username is email use as login for authentication
- password must be at least 8 characters and contains one digit, one uppercase, one lowercase and one specific character
- password_confirm must be identical to password

### <u>Login</u>

Route: (POST) http://localhost/api/login

Body :

    {
        "username": string,
        "password": string
    }

Returns a JWT access token

### <u>Get account information</u>

It's a protected route, so you must provide a JWT token in Authorization headers

Route: (GET) http://localhost/api/user/me

Returns your account information with all groups you belong to

### <u>Join the specified group of pokemon type</u>

It's a protected route, so you must provide a JWT token in Authorization headers

Route: (POST) http://localhost/api/group/{{type}}/add

Body :

### <u>Leave the specified group of pokemon type</u>

It's a protected route, so you must provide a JWT token in Authorization headers

Route: (POST) http://localhost/api/group/{{type}}/remove

Body :

### <u>List all pokemons of groups of type you belong to</u>

It's a protected route, so you must provide a JWT token in Authorization headers

Route: (GET) http://localhost/api/pokemon

### <u>Get details of the given pokemon if his type(s) is/are in your groups</u>

It's a protected route, so you must provide a JWT token in Authorization headers

Route: (GET) http://localhost/api/pokemon/{{pokemon}}
