import importlib
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from passlib.hash import argon2

password_hasher = argon2.using(rounds=2, parallelism=1, memory_cost=14649)


def init_db(app: Flask, db: SQLAlchemy):
    with app.app_context():
        first_init = True
        for t in db.metadata.sorted_tables:
            if db.inspect(db.engine).has_table(t):
                first_init = False

        if first_init:
            gbl = globals()
            for module in os.listdir(os.path.join(app.config.get("BASE_DIR"), "models")):
                if not module.startswith("__"):
                    if module.endswith(".py"):
                        model = module.replace(".py", "")
                        gbl[model] = importlib.import_module(f"models.{model}")

            db.create_all()