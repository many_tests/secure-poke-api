class ValidationError(Exception):
    """Raised when validation of given field is not correct."""

    def __init__(self, field: str, message: str):
        self.status_code = 400
        self.message = message
        self.field = field
        super().__init__()

    def normalized_messages(self) -> dict:
        return {self.field: self.message}
