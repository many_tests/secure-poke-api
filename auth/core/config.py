"""
All environment Config
"""
import os
from pydantic import BaseSettings
from enum import Enum


class DatabaseSettings(BaseSettings):
    """
    Configuration for database connection
    """

    scheme: str = "postgresql+psycopg2"
    name: str
    host: str
    port: str
    user: str
    password: str

    def uri(self) -> str:
        return f"{self.scheme}://{self.user}:{self.password}@{self.host}:{self.port}/{self.name}"


class EnvironmentEnum(str, Enum):
    """
    Enum with available environments
    """

    PRODUCTION = "production"
    DEVELOPMENT = "development"


class GlobalConfig(BaseSettings):
    """
    Global config for override in relation of env
    """

    TITLE: str = "Secure Poké API Auth server"
    DESCRIPTION: str = "Web service to manage user authentication"
    VERSION: str = "0.0.1"

    BASE_DIR: str = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    DEBUG: bool = False
    SECRET_KEY: str

    SQLALCHEMY_DATABASE_URI: str
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False

    TOKEN_EXPIRE_DELAY: int = 15  # in minutes

    class Config:
        case_sensitive = True


class DevelopmentConfig(GlobalConfig):
    """Development configurations."""

    DEBUG: bool = True
    SECRET_KEY: str = "very_secret"

    SQLALCHEMY_DATABASE_URI: str = DatabaseSettings(
        name=os.getenv("POSTGRES_DB"),
        host=os.getenv("POSTGRES_HOST"),
        port=os.getenv("POSTGRES_PORT"),
        user=os.getenv("POSTGRES_USER"),
        password=os.getenv("POSTGRES_PASSWORD"),
    ).uri()


class ProductionConfig(GlobalConfig):
    """Production configurations."""

    DEBUG: bool = False
    SECRET_KEY: str = os.getenv("SECRET", "very_secret")
    SQLALCHEMY_DATABASE_URI: str = DatabaseSettings(
        name=os.getenv("POSTGRES_DB"),
        host=os.getenv("POSTGRES_HOST"),
        port=os.getenv("POSTGRES_PORT"),
        user=os.getenv("POSTGRES_USER"),
        password=os.getenv("POSTGRES_PASSWORD"),
    ).uri()


def get_configuration(env) -> GlobalConfig:
    """
    Instantiation of configuration from env variable
    """
    if env == EnvironmentEnum.PRODUCTION.value:
        return ProductionConfig()
    return DevelopmentConfig()
