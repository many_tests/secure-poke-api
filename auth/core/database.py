from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()


class BaseModel(db.Model):
    """Default base models for embedded commit to db"""

    __tablename__ = None
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    def __init__(self, **kwargs):
        super(BaseModel, self).__init__(**kwargs)

    def save(self):
        """Save an instance of the model from the database."""
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except Exception as err:
            db.session.rollback()
            raise err

    def update(self):
        """Update an instance of the model from the database."""
        db.session.commit()
        return self

    def delete(self):
        """Delete an instance of the model from the database."""
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as err:
            db.session.rollback()
            raise err
