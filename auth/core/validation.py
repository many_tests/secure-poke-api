import re


def email_validation(value):
    if re.match(r"^[\w\.\+\-]+\@[\w_-]+\.[a-z]{2,3}$", value):
        return True
    return False


def password_validation(value):
    spec_chars = ["-", "_", ".", "!", "@", "#", "$", "^", "&", "(", ")", "[", "]"]
    if len(value) < 6 or len(value) > 20:
        raise AssertionError("Password length is not in range of 6 to 20 char")
    elif not any(char.isdigit() for char in value):
        raise AssertionError("Password not contain digit")
    elif not any(char.isupper() for char in value):
        raise AssertionError("Password not contain uppercase")
    elif not any(char.islower() for char in value):
        raise AssertionError("Password not contain lowercase")
    elif not any(char in spec_chars for char in value):
        raise AssertionError(f"Password not contain specific char : {spec_chars}")
    else:
        return True
