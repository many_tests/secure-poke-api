"""
Module docstring
"""
from sqlalchemy.orm import validates

from core.database import BaseModel, db, ma
from core.exception import ValidationError
from core.utils import password_hasher
from core.validation import email_validation, password_validation


class User(BaseModel):
    """
    Class docstring
    """

    __tablename__ = "user"

    name = db.Column(db.String(250))
    email = db.Column(db.String(250), unique=True)
    password = db.Column(db.String(250))

    # relations
    groups = db.relationship('Group', backref='user')

    def __init__(self, name: str, email: str, password: str, **kwargs):
        """
        Docstring
        :param name:
        :param email:
        :param password:
        :param kwargs:
        """
        super(User, self).__init__(**kwargs)
        self.name = name
        self.email = email
        self.password = password

    @validates("email")
    def validate_email(self, key: str, email: str) -> str:
        if not email_validation(email):
            raise ValidationError(key, "Email not valid.")
        if self.id is None and User.query.filter_by(email=email).first():
            raise ValidationError(key, "Email is already use.")
        if email != self.email and User.query.filter_by(email=email).first():
            raise ValidationError(key, "Email is already use.")
        return email

    @validates("password")
    def validate_password(self, key: str, password: str) -> str:
        try:
            if password_validation(password):
                return password_hasher.hash(password)
        except AssertionError as err:
            raise ValidationError(key, str(err))
