"""
Module docstring
"""
from typing import Union

from sqlalchemy.orm import validates

from core.database import BaseModel, db
from core.exception import ValidationError
from models.user import User
from services.poke import PokeAPI
from services.poke.exception import PokeTypeNotFound


class Group(BaseModel):
    """
    Class docstring
    """

    __tablename__ = "group"

    name = db.Column(db.String(250))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))

    def __init__(self, name: str, user: Union[int, User], **kwargs):
        """
        Docstring
        :param name:
        :param user:
        :param kwargs:
        """
        super().__init__(**kwargs)
        self.name = name
        self.user_id = user if isinstance(user, int) else user.id

    @validates("name")
    def validate_name(self, key: str, name: str) -> str:
        try:
            PokeAPI().get_type(name)
        except PokeTypeNotFound as e:
            raise ValidationError(key, e.message)

        return name
