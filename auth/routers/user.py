from flask import Blueprint
from flask_jwt_extended import jwt_required, get_jwt_identity

from models.user import User
from ressources.user import user_sc

user_bp = Blueprint("user_bp", __name__, url_prefix="/api/user")


@user_bp.route('/me', methods=['GET'])
@jwt_required()
def me():
    user = User.query.filter_by(email=get_jwt_identity()).first()
    return user_sc.dump(user), 200
