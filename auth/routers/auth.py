from flask import request, jsonify, Blueprint
from flask_jwt_extended import create_access_token

from core.exception import ValidationError
from core.utils import password_hasher
from models.user import User
from ressources.user import user_sc
from ressources.auth import AuthSchema, SignUpSchema

auth_bp = Blueprint("auth_bp", __name__, url_prefix="/api")


@auth_bp.route('/login', methods=['POST'])
def login():
    data = AuthSchema(**request.get_json())
    user = User.query.filter_by(email=data.username).first()

    if not user:
        return jsonify({"detail": f"Error: no user found for {data.username}, please sign up."}), 404

    if not password_hasher.verify(data.password, user.password):
        return jsonify({"detail": "Error: wrong password"}), 400

    access_token = create_access_token(identity=data.username)
    return jsonify(access_token=access_token)


@auth_bp.route('/signup', methods=['POST'])
def signup():
    data = SignUpSchema(**request.get_json())

    try:
        user = User(name=data.name, email=data.email, password=data.password)
    except ValidationError as e:
        return jsonify(e.normalized_messages()), e.status_code

    user.save()
    return jsonify(user_sc.dump(user)), 201
