from flask import Blueprint
from flask_jwt_extended import jwt_required, get_jwt_identity

from core.exception import ValidationError
from models.group import Group
from models.user import User

grp_bp = Blueprint("grp_bp", __name__, url_prefix="/api/group")


@grp_bp.route('/<type>/add', methods=['POST'])
@jwt_required()
def add_user_to_group(type: str):
    user = User.query.filter_by(email=get_jwt_identity()).first()
    if type in [g.name for g in user.groups]:
        return {"detail": f"You have already join the Pokemon group of type {type}"}, 403
    try:
        Group(name=type, user=user).save()
    except ValidationError as e:
        return e.normalized_messages(), e.status_code
    return {"detail": f"You have join the Pokemon group of type {type}"}, 200


@grp_bp.route('/<type>/remove', methods=['POST'])
@jwt_required()
def remove_user_to_group(type: str):
    user = User.query.filter_by(email=get_jwt_identity()).first()
    if type not in [g.name for g in user.groups]:
        return {"detail": f"You can't leave the pokemon group of type {type}, you haven't join it"}, 400
    Group.query.filter_by(name=type, user=user).first().delete()
    return {"detail": f"You have leave the Pokemon group of type {type}"}, 200
