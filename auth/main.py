from flask import Flask

from core.authentication import jwt_manager
from core.config import get_configuration
from core.database import db, ma
from core.utils import init_db


def create_app(**kwargs) -> Flask:
    """
    Create App instance with bdd connection and blueprints registering

    :return: app
    """
    # create and configure the app
    _app = Flask(__name__, instance_relative_config=True)

    # Init authentication
    jwt_manager.init_app(_app)

    # Load config
    _app.config.from_object(get_configuration(_app.config["ENV"]))
    _app.url_map.strict_slashes = False

    # Init db
    db.init_app(_app)
    ma.init_app(_app)

    init_db(_app, db)

    # Init Resources
    from routers.auth import auth_bp
    from routers.user import user_bp
    from routers.group import grp_bp

    _app.register_blueprint(auth_bp)
    _app.register_blueprint(user_bp)
    _app.register_blueprint(grp_bp)

    return _app


if __name__ == "__main__":
    app = create_app()
