from core.database import ma
from models.user import User
from ressources.group import GroupSchema


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        ordered = True

    password = ma.auto_field(load_only=True)
    groups = ma.Nested(GroupSchema, many=True)


user_sc = UserSchema()
users_sc = UserSchema(many=True)
