from core.database import ma
from models.group import Group


class GroupSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Group
        ordered = True


group_sc = GroupSchema()
groups_sc = GroupSchema(many=True)
