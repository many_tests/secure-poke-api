"""
Group all schema use in authentication
"""
from pydantic import BaseModel, Field, validator

from core.exception import ValidationError


class AuthSchema(BaseModel):
    """Schema for login payload"""
    username: str
    password: str


class SignUpSchema(BaseModel):
    """Schema for signup payload"""
    name: str
    email: str = Field(alias="username")
    password: str
    password_confirm: str

    @validator('password_confirm')
    def passwords_match(cls, v: str, values: dict, **kwargs) -> str:
        if 'password' in values and v != values['password']:
            raise ValidationError("password_confirm", "Not similar passwords")
        return v
